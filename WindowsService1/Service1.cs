﻿using Nancy.Json;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace WindowsService1
{
    public partial class BookRadarSettlementService : ServiceBase
    {
        System.Timers.Timer timer = new System.Timers.Timer(); // name space(using System.Timers;)  
        public BookRadarSettlementService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 10000; //number in milisecinds  

            timer.Enabled = true;
        }
        protected override void OnStop()
        {
            // WriteToFile("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {

            this.LogMessage("Service Running");
        }
        private void LogMessage(string Message)
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            try
            {
                connection = new SqlConnection(@"Server=188.241.120.123,1533;Initial Catalog=BookRadar_Live;Persist Security Info=True;User ID=user_bookradar_live;Password=Bookradarlive@111;MultipleActiveResultSets=False;Connection Timeout=30;");
                command = new SqlCommand("select * from Market where MarketStatus = 4 and IsSettled = 0 and Result <> '' and Result IS NOT NULL", connection);
                connection.Open();
                SqlDataAdapter adp = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string marketType = dt.Rows[i]["marketType"].ToString(); // Change Field Name 
                    if (marketType == "LINE")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleLineMarket", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleLineMarket", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "AdvanceSession")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleSession", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleSession", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "Bookmakers")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleBookMakers", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleBookMakers", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleMatchOdds", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleMatchOdds", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Èxception In Settlement", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                command.Dispose();
                connection.Dispose();
            }
        }

    }
}
